/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"bytes"
	"context"
	"time"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	log "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/predicate"

	"github.com/go-logr/logr"
	helmClient "github.com/mittwald/go-helm-client"
	goacademytsystemscomv1 "gitlab.com/mbolanovsky/operator/api/v1"
	appv1 "k8s.io/api/apps/v1"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

// MbolanovskyReconciler reconciles a Mbolanovsky object
type MbolanovskyReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

const myFinalizerName string = "finalizer"

//+kubebuilder:rbac:groups=goacademy.t-systems.com,resources=mbolanovskies,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=goacademy.t-systems.com,resources=mbolanovskies/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=goacademy.t-systems.com,resources=mbolanovskies/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Mbolanovsky object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile

func (r *MbolanovskyReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx)

	logger.Info("start processing")
	// TODO(user): your logic here
	var mbolanovsky goacademytsystemscomv1.Mbolanovsky
	err := r.Client.Get(ctx, req.NamespacedName, &mbolanovsky)
	if err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}
	logger.Info(mbolanovsky.Spec.Namespace)

	var outputBuffer bytes.Buffer

	opt := &helmClient.Options{
		Namespace:        mbolanovsky.Spec.Namespace, // Change this to the namespace you wish the client to operate in.
		RepositoryCache:  "/tmp/.helmcache",
		RepositoryConfig: "/tmp/.helmrepo",
		Debug:            true,
		Linting:          true,
		DebugLog:         func(format string, v ...interface{}) {},
		Output:           &outputBuffer, // Not mandatory, leave open for default os.Stdout
	}

	// Define the chart to be installed
	chartSpec := helmClient.ChartSpec{
		ReleaseName: "server-gin-operator",
		ChartName:   mbolanovsky.Spec.Helmchartpath,
		Namespace:   mbolanovsky.Spec.Namespace,
		UpgradeCRDs: true,
		Wait:        true,
		Timeout:     5 * time.Minute,
	}

	helmClient, err := helmClient.New(opt)
	if err != nil {
		panic(err)
	}

	if !mbolanovsky.DeletionTimestamp.IsZero() {
		return r.UninstallHelmChart(logger, helmClient, ctx, chartSpec, &mbolanovsky)
	} else if mbolanovsky.Spec.Helmchartpath != mbolanovsky.Status.InstalledHelmChart {
		return r.InstallHelmChart(logger, helmClient, ctx, chartSpec, &mbolanovsky)
	} else if mbolanovsky.Spec.Restart != mbolanovsky.Status.Restart {
		return r.Restart(logger, ctx, &mbolanovsky)
	}

	return ctrl.Result{}, nil

}

// SetupWithManager sets up the controller with the Manager.
func (r *MbolanovskyReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&goacademytsystemscomv1.Mbolanovsky{}).
		WithEventFilter(predicate.GenerationChangedPredicate{}).
		Complete(r)
}

func (r *MbolanovskyReconciler) UninstallHelmChart(logger logr.Logger, helmClient helmClient.Client, ctx context.Context, chartSpec helmClient.ChartSpec, mbolanovsky *goacademytsystemscomv1.Mbolanovsky) (ctrl.Result, error) {
	logger.Info("UNINSTALL")
	//helmClient.UninstallReleaseByName(chartSpec.ReleaseName)
	if err := helmClient.UninstallRelease(&chartSpec); err != nil {
		return ctrl.Result{}, err
	}
	patch := client.MergeFrom(mbolanovsky.DeepCopy())
	// The object is being deleted
	if controllerutil.ContainsFinalizer(mbolanovsky, myFinalizerName) {
		// remove our finalizer from the list and update it.
		controllerutil.RemoveFinalizer(mbolanovsky, myFinalizerName)
		err := r.Patch(ctx, mbolanovsky, patch)
		return ctrl.Result{}, err
	}
	return ctrl.Result{}, nil
}

func (r *MbolanovskyReconciler) InstallHelmChart(logger logr.Logger, helmClient helmClient.Client, ctx context.Context, chartSpec helmClient.ChartSpec, mbolanovsky *goacademytsystemscomv1.Mbolanovsky) (ctrl.Result, error) {
	logger.Info("INSTALL")
	// Install a chart release.
	// Note that helmclient.Options.Namespace should ideally match the namespace in chartSpec.Namespace.
	if _, err := helmClient.InstallOrUpgradeChart(context.Background(), &chartSpec, nil); err != nil {
		return ctrl.Result{}, err
	}
	patch := client.MergeFrom(mbolanovsky.DeepCopy())
	// The object is not being deleted, so if it does not have our finalizer,
	// then lets add the finalizer and update the object. This is equivalent registering our finalizer.
	if !controllerutil.ContainsFinalizer(mbolanovsky, myFinalizerName) {
		controllerutil.AddFinalizer(mbolanovsky, myFinalizerName)
		if err := r.Patch(ctx, mbolanovsky, patch); err != nil {
			return ctrl.Result{}, err
		}
	}

	patch = client.MergeFrom(mbolanovsky.DeepCopy())
	mbolanovsky.Status.InstalledHelmChart = mbolanovsky.Spec.Helmchartpath
	if err := r.Status().Patch(ctx, mbolanovsky, patch); err != nil {
		return ctrl.Result{}, err
	}

	return ctrl.Result{Requeue: true}, nil
}

func (r *MbolanovskyReconciler) Restart(logger logr.Logger, ctx context.Context, mbolanovsky *goacademytsystemscomv1.Mbolanovsky) (ctrl.Result, error) {
	logger.Info("RESTART")
	var dpl appv1.Deployment
	err2 := r.Client.Get(ctx, types.NamespacedName{Name: "webserver", Namespace: mbolanovsky.Spec.Namespace}, &dpl)
	if err2 != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err2)
	}

	if dpl.Spec.Template.ObjectMeta.Annotations == nil {
		dpl.Spec.Template.ObjectMeta.Annotations = make(map[string]string)
	}
	dpl.Spec.Template.ObjectMeta.Annotations["restartAt"] = time.Now().Format(time.RFC3339)
	err := r.Client.Update(ctx, &dpl)
	if err != nil {
		return ctrl.Result{}, err
	}
	patch := client.MergeFrom(mbolanovsky.DeepCopy())
	// The object is not being deleted, so if it does not have our finalizer,
	// then lets add the finalizer and update the object. This is equivalent registering our finalizer.
	mbolanovsky.Status.Restart = mbolanovsky.Spec.Restart
	if err := r.Status().Patch(ctx, mbolanovsky, patch); err != nil {
		return ctrl.Result{}, err
	}
	return ctrl.Result{}, nil
}

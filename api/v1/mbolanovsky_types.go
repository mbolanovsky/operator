/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// MbolanovskySpec defines the desired state of Mbolanovsky
type MbolanovskySpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Foo is an example field of Mbolanovsky. Edit mbolanovsky_types.go to remove/update
	//+kubebuilder:validation:Pattern:=^/tmp/.+\.tgz$
	Helmchartpath string      `json:"helmchartpath,omitempty"`
	Namespace     string      `json:"namespace"`
	Restart       metav1.Time `json:"restart"`
}

// MbolanovskyStatus defines the observed state of Mbolanovsky
type MbolanovskyStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Restart            metav1.Time `json:"restart,omitempty"`
	InstalledHelmChart string      `json:"installedHelmChart"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// Mbolanovsky is the Schema for the mbolanovskies API
type Mbolanovsky struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   MbolanovskySpec   `json:"spec,omitempty"`
	Status MbolanovskyStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// MbolanovskyList contains a list of Mbolanovsky
type MbolanovskyList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Mbolanovsky `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Mbolanovsky{}, &MbolanovskyList{})
}

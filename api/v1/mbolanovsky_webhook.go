/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	"fmt"
	"os"

	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
)

// log is for logging in this package.
var mbolanovskylog = logf.Log.WithName("mbolanovsky-resource")

func (r *Mbolanovsky) SetupWebhookWithManager(mgr ctrl.Manager) error {
	return ctrl.NewWebhookManagedBy(mgr).
		For(r).
		Complete()
}

// TODO(user): EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!

//+kubebuilder:webhook:path=/mutate-goacademy-t-systems-com-v1-mbolanovsky,mutating=true,failurePolicy=fail,sideEffects=None,groups=goacademy.t-systems.com,resources=mbolanovskies,verbs=create;update,versions=v1,name=mmbolanovsky.kb.io,admissionReviewVersions=v1

var _ webhook.Defaulter = &Mbolanovsky{}

// Default implements webhook.Defaulter so a webhook will be registered for the type
func (r *Mbolanovsky) Default() {
	mbolanovskylog.Info("default", "name", r.Name)
	if r.Spec.Helmchartpath == "" {
		r.Spec.Helmchartpath = "/tmp/martin-chart-0.1.0.tgz"
	}

	// TODO(user): fill in your defaulting logic.
}

// TODO(user): change verbs to "verbs=create;update;delete" if you want to enable deletion validation.
//+kubebuilder:webhook:path=/validate-goacademy-t-systems-com-v1-mbolanovsky,mutating=false,failurePolicy=fail,sideEffects=None,groups=goacademy.t-systems.com,resources=mbolanovskies,verbs=create;update,versions=v1,name=vmbolanovsky.kb.io,admissionReviewVersions=v1

var _ webhook.Validator = &Mbolanovsky{}

// ValidateCreate implements webhook.Validator so a webhook will be registered for the type
func (r *Mbolanovsky) ValidateCreate() error {
	mbolanovskylog.Info("validate create", "name", r.Name)
	file, err := os.Stat(r.Spec.Helmchartpath)
	if err != nil {
		return err
	}

	if !file.Mode().IsRegular() {
		return fmt.Errorf("%s is not regular file", r.Spec.Helmchartpath)
	}

	return nil
}

// ValidateUpdate implements webhook.Validator so a webhook will be registered for the type
func (r *Mbolanovsky) ValidateUpdate(old runtime.Object) error {
	mbolanovskylog.Info("validate update", "name", r.Name)
	file, err := os.Stat(r.Spec.Helmchartpath)
	if err != nil {
		return err
	}

	if !file.Mode().IsRegular() {
		return fmt.Errorf("%s is not regular file", r.Spec.Helmchartpath)
	}

	return nil
}

// ValidateDelete implements webhook.Validator so a webhook will be registered for the type
func (r *Mbolanovsky) ValidateDelete() error {
	mbolanovskylog.Info("validate delete", "name", r.Name)

	// TODO(user): fill in your validation logic upon object deletion.
	return nil
}
